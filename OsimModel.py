#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:31:14 2020

@author: elandais
"""
import sys
import opensim as osim
import numpy as np
import os
import pyosim.algebra as alg
import random

import time

# method create TRC file
# nb : c3d2osim : https://github.com/dvarnai/pyC3D2OSIM

class OsimModel():
    """!
    Class creating a Osim Model, saving and updating useful informations about it.
    """
    def __init__(self,model_path, geometry_path="", ik_path="", setup_ik_file="", trc_path="", visualize=False):
        """!
        
        Constructor of the OsimModel class.

        Args:
            model_path (String):Absolute path to the .osim file (with the .osim filename).

            geometry_path (String, optional):Path to the .vtp files associated with the .osim model.

            ik_path (String, optional): Path to the Inverse Kinematics configuration file. The default is "".

            setup_ik_file(String, optional):Name of the Inverse Kinematics configuration file. This file is used
                to set parameters of the Inverse Kinematics operation (weight of each
                markers of the model for example). The default is "".
            trc_path(String, optional):Path to a trc file associated with the .osim file. This file contains
                the different positions of each one of the markers of the model, at
                each time. This file is then used to determine the position of each
                one of the joints of the model at each time. The default is "". 
            visualize : Boolean, optional
                Activate the visualization of the model. The default is False.

        """
        
        ## Absolute path to the .osim file (with the .osim filename).
        self.model_path = model_path
        
        ## Absolute path to the Inverse Kinematics configuration file.
        self.compl_ik_path = ""
        
        ## Absolute path to the output motion file of the Inverse Kinematics.
        self.motion_file = ""
        
        if len(ik_path) != 0:
            try:
                os.mkdir(ik_path+"/output_IK/")
            except OSError:
                pass 
            if len(setup_ik_file)!=0:
                self.compl_ik_path = ik_path+"/"+setup_ik_file
            if len(trc_path)!=0:
                motion_name= trc_path.split("/")[-1].split(".")[0]
                self.motion_file = ik_path+"/output_IK/" + motion_name + ".mot"        
        
        ## Absolute path to the .trc file.
        self.trc_path= trc_path
        
        ## Absolute path to the .vtp files.
        self.geometry_path = geometry_path
        
        ## Object describing a .osim model, from the opensim module.
        self.osimModel = osim.Model(self.model_path)

        if visualize : 
            self.osimModel.setUseVisualizer(True)
            osim.ModelVisualizer.addDirToGeometrySearchPaths(geometry_path)
        
        ## Object describing the state of a .osim model, from the opensim module.
        self.osimState = self.osimModel.initSystem()
        
        ## Dictionary expressing, for each body part, its position and rotation matrix into the Ground referential.
        self.D_body_pos = {}
        
        ## Dictionary expressing, for each coordinate object, its value.
        self.D_joints_value = {}
        
        ## Dictionary expressing, for each joint, its position and rotation matrix in the global referential Ground.
        self.D_joints_pos = {}
        
        ## Dictionary expressing, for each joint, its position and rotation matrix in the referential of the parent joint.
        ## (Those values should be fixed)
        self.D_jp = {}    
        
        self.D_jp = self.getJointsPositions("Parent",n=0)     

        ## Dictionary linking the names of the coordinates objects to the name of their respective joints.
        self.D_coord_joints= {}
        
        ## Dictionary linking the names of the joints to the names of the coordinates objects associated with these joints. 
        self.D_joints_coord = {}
        
        self.D_coord_joints,self.D_joints_coord = self.getCorresJointsCoord()           

        ## Dictionary linking the name of the coordinate objects with their minimum and maximum amplitude on their DOF.
        self.D_joints_range = self.getJointsLimits()
        
        ## Dictionary linking the index associated with the value of the joints of the model, for the variable coordinateSet and the variable Q.
        self.D_Q_coord = self.getCorresQCoord()
        
        ## Dictionary containing, for each of the coordinates (= DOF) names of the Ground joint, the type and the axis associated with this coordinate.
        # self.D_gd_prop = self.getGroundProperties()
        
        self.updModel()
        
    def updModel(self):
        """!
        
        Method used to update the different variables of the OsimModel class.
        This method should be called after each operation on the state of the
        .osim model.
        """
        self.D_joints_value = self.getJointsValues()
        self.D_body_pos = self.getBodiesPositions()
        self.D_joints_pos = self.getJointsPositions("Ground",n=1)        

    def runIK(self):
        """!
        
        Method used to execute the Inverse Kinematics operation. The variables 
        motion_file (=path to the output motion file of Inverse Kinematics), 
        compl_ik_path (= path to the Inverse Kinematics configuration file) and 
        trc_path (= path to the marker file used by Inverse Kinematics) should 
        be defined first. 
        Those variables are defined at the constructor step if the variables ik_path,
        setup_ik_file and trc_path were defined.

        """
        if len(self.motion_file) == 0 :
            print("Error : motion_file (=path to the output motion file of Inverse Kinematics) variable not defined")
        elif len(self.compl_ik_path) == 0 :
            print("Error : compl_ik_path (= path to the Inverse Kinematics configuration file) variable not defined")
        elif len(self.trc_path) == 0:
            print("Error : trc_path (= path to the marker file used by Inverse Kinematics) variable not defined")            
        else:
            trc_path.ikFromFile(self.trc_path,self.compl_ik_path,self.motion_file)
            self.updModel()
    
    def setQ(self, LQ, LQtype = 0):
        """!
        
        Set the Q vector of the .osim model, containing the values of each joint
        of this model. Those values are set in accordance to a list of variables
        LQ. 

        Args:
            LQ : List.
                List containing the values of each joint of the model.
                
            LQtype : Int.
                Define the configuration of the LQ list.
                
                Type 0 : Configuration according to the order of the CoordinateSet
                variable of the .osim Model.
                LQ[i] corresponds to the desired value of CoordinateSet[i]. As there
                is no direct link between CoordinateSet[i] and Q[i], a transformation is
                necessary.
                
                Type 1 : Configuration according to the order of Q vector used by 
                osimState.
                LQ[i] corresponds to the desired value of Q[i] used by osimState. 
                Here, no transformation is necessary.            
                
                The default is 0.

        """
        
        RQ = []
        # set different configuration of the vector Q used for the .osim model.
        if LQtype ==0:
            for k in range(len(LQ)):
                RQ.append( LQ[self.D_Q_coord[k]] )
        elif LQtype == 1:
            RQ = LQ
        
        # set Q for the osim model
        self.__setQVals(RQ)
        # update the variables of the OsimModel class.
        self.updModel()
         
    def getGroundProperties(self):
        """!
        
        Method used to gather some useful properties about the Ground link
        of the .osim model. 

        Returns:
            D_gd_prop : Dictionary of list.
                Dictionary containing, for each of the coordinates (= DOF) names of the 
                Ground joint, the type and the axis associated with this coordinate.

        """
        jointlist = self.osimModel.getJointList()
        gd_j = None
        D_gd_prop = {"joint_name" : "",
                    "joint_type": "",
                    "properties" : {}}
        
        for joint in jointlist:
            p_frame = joint.get_frames(0)
            if type(gd_j)==type(None) and "ground" in p_frame.getName():
                gd_j = joint

        if gd_j != None:
            D_gd_prop["joint_name"] = gd_j.getName()
            
            type_joint = str( type(gd_j) )[:-2].split(".")[-1]
                    
            D_gd_prop["joint_type"] = type_joint
            if type_joint == "CustomJoint":

                trans = gd_j.getSpatialTransform()
                
                rot1 = trans.get_rotation1()
                rot2 = trans.get_rotation2()
                rot3 = trans.get_rotation3()
                
                tr1 = trans.get_translation1()
                tr2 = trans.get_translation2()
                tr3 = trans.get_translation3()
                
                L_trans = [rot1,rot2,rot3,tr1,tr2,tr3]
                L_trans_name = ["rotation1",
                                "rotation2",
                                "rotation3",
                                "translation1",
                                "translation2",
                                "translation3"]
                
                axes = [  self.numpy(x.getAxis() ) for x in L_trans ]
                names_axes = [ self.numpy(x.getCoordinateNamesInArray()) for x in L_trans]
        
                for typ,axe,name in zip(L_trans_name,axes,names_axes):
        
                    D_gd_prop["properties"][name[0,0]] = [typ,axe]
                    
        return(D_gd_prop)
    
    def getCorresQCoord(self):
        """!
        
        Method used to get the correspondance between the index associated with
        the value of the joints of the model, for the variable coordinateSet 
        and the variable Q.

        Returns:
            D_Q_coord : Dictionary.   
                D_Q_coord is organised as follows : 
                    D_Q_coord[i] = j <--->  Q[i] = coordinateSet[j].getValue(osimState).
                
                The value of coordinate j (wrt CoordinateSet) corresponds to the i-th component
                of Q from osimState.

        """
        coordinateSetlist = self.osimModel.getCoordinateSet()
        
        D_Q_coord = {}
        
        NQ = self.osimState.getNQ()
        
        LQ = [0. for k in range(NQ)]    
        for i in range(NQ):

            LQ[i] = 0.25
            self.__setQVals(LQ)
            j  = 0
            for coord in coordinateSetlist:
                val = coord.getValue(self.osimState)
                if val == 0.25:
                    break
                else:
                    j+=1
            D_Q_coord[i] = j
            LQ[i] = 0.
        return(D_Q_coord)
   
    def printCorresQCoord(self):
        """!
        
        Method used to print the correspondance between the index associated with
        the value of the joints of the model, for the variable coordinateSet 
        and the variable Q. This method is based on the method getCorresQCoord.

        """
        D_Q_coord = self.getCorresQCoord()
        
        coordinateSetlist = self.osimModel.getCoordinateSet()

        l=0
        for coord in coordinateSetlist:
            name = coord.getName()
            print("Name : {:<24} : index in coordinateSetlist : {: d} | Q : {:d}"
                .format(name, l, D_Q_coord[l]))
            l+=1

    def getJointsLimits(self):
        """!
        
        Method used to get the limits values of each one of the DOF of the model.

        Returns:
            D_joints_range : Dictionary of List
                D_joints_range is organised as follows : 
                    D_joints_range[i] = [lower limit of DOF i, upper limit of DOF i].

        """
        coordinateSetlist = self.osimModel.getCoordinateSet()
        D_joints_range = {}  

        for coord in coordinateSetlist:
            name = coord.getName()
            lim = [coord.get_range(0),coord.get_range(1) ]
            D_joints_range[name] = lim
        return(D_joints_range)

    def printJointsLimits(self):
        """!
        
        Method used to print the limits values of each one of the DOF of the model,
        based on the method getJointsLimits

        """
        D_joints_range = self.getJointsLimits()
        
        print("--- Limits (lower upper) of each articulation ---")
        for name in D_joints_range:
            lim = D_joints_range[name]
            print("Name {:<24} : {: .4f} |  {: .4f}".format(name,lim[0], lim[1]))

    def getCorresJointsCoord(self):
        """!
        
        Method used to get a quick access to the correspondances between the joints
        of the model and the coordinates associated with each one of those joints.
        
        A coordinate is associated to an unique joint, but a joint can have multiple
        coordinates. 

        Returns:
            D_coord_joints : Dictionary.
                Dictionary linking the names of the coordinates objects to the name of
                their respective joints.
                Ex : D_coord_joints[pelvis_tx] = "pelvis"
                    D_coord_joints[pelvis_ty] = "pelvis"
                    D_coord_joints[pelvis_tz] = "pelvis"
                    D_coord_joints[pelvis_rx] = "pelvis"
                    D_coord_joints[pelvis_ry] = "pelvis"
                    D_coord_joints[pelvis_rz] = "pelvis"
    
            D_joints_coord : Dictionary. 
                Dictionary linking the names of the joints to the names of the coordinates
                objects associated with these joints. 
                Ex : D_joints_coord[pelvis] = [pelvis_tx,pelvis_ty,pelvis_tz,pelvis_rx,pelvis_ry,pelvis_rz]
        
        """
        coordinateSetlist = self.osimModel.getCoordinateSet()

        D_joints_coord = {}
        D_coord_joints = {}

        for coord in coordinateSetlist:
            coord_name = coord.getName()
            coord_joint = coord.getJoint()
            
            joint_name = coord_joint.getName()

            if joint_name not in D_joints_coord:
                D_joints_coord[joint_name] = [coord_name]
            else:
                D_joints_coord[joint_name].append(coord_name)
            D_coord_joints[coord_name] = joint_name
        
        # Some special joints may not have any coordinates (ex : ground)
        # This is used to deal with this special case. 
        jointlist = self.osimModel.getJointList()
        for joint in jointlist:
            joint_name = joint.getName()
            if joint_name not in D_joints_coord:
                D_joints_coord[joint_name] = [joint_name]
                D_coord_joints[joint_name] = joint_name

        return(D_coord_joints,D_joints_coord)
    
    def printJointsDivisionsIntoOneDOF(self):
        """!
        
        Method used to print the correspondances between the joints
        of the model and the coordinates associated with each one of those joints.
        Based on the method getCorresJointsCoord.

        """
        print("--- Division of each joints into one DOF  ---")
        
        D_coord_joints, D_joints_coord = self.getCorresJointsCoord()
        
        for joint_name in D_joints_coord:
            print("Name of the joint : %s ; elements division : "%joint_name, D_joints_coord[joint_name])
        
    def getJointsPositions(self,refe="Ground",n=1):
        """!
        Method used to get the positions of one of the two frames of the joints
        of the model. The positions of the selected frames can then be expressed into 
        the global referential "Ground" of the model, or the referential of the parent
        joint "Parent" of the selected joint.

        Args:
            refe : String, optional.
            
                Type of referential chosen to express the joints position. Two choices
                are available : 
                
                refe = "Ground", to express the positions into the global referential 
                Ground. 
                
                refe = "Parent", to express the positions into the referential of the parent
                joint of the selected joint. This could be useful to determine the position 
                of the joint compared to the previous joint.
                    
                The default is "Ground".
            
            n : Int, optional.
            
                Value associated with the joint frame to be selected for each of the 
                joints.
                Two frames are available : one frame fixed in relation to the parent joint, 
                the other fixed in relation to the current joint. 
                
                n = 0 : frame fixed in relation to the parent joint.
                
                n = 1 : frame fixed in relation to the current joint.
            
                The default is 1.
        
        Returns:
            D_joints_pos : Dictionary of list.
                Dictionary expressing, for each joint, its position and rotation matrix 
                in the chosen referential. 
                
                Example : 
                    D_joints_pos[pelvis] = [ M_p, M_R ]
                    M_p = np.array([[0.],
                                    [2.],
                                    [0.]])
                    M_R = np.eye(3,3)

        """
        jointlist = self.osimModel.getJointList()
        D_joints_pos = {}
        for joint in jointlist:
            name = joint.getName()
            p_frame = joint.get_frames(n)
            if refe == "Ground":
                p = p_frame.getTransformInGround(self.osimState).p()
                R = p_frame.getTransformInGround(self.osimState).R().asMat33()
            elif refe == "Parent":
                pp_frame = p_frame.getParentFrame()
                transf = p_frame.findTransformBetween(self.osimState,pp_frame)
                p = transf.p()
                R = transf.R().asMat33()
            M_R = self.numpy(R)      
            M_p = self.numpy(p)
            D_joints_pos[name] = [M_p,M_R]
        return(D_joints_pos)
       
    def simulateModel(self,time = 0.01, trc_path = None,compl_ik_path = None):
        """!

        (BETA). Method used to simulate and visualize a the  model           

        Args:
                
            time: float64
                    Duration of the simualtion - by default 10ms, very short

            trc_path : String
                    Path to a trc file associated with the .osim file. This file contains
                    the different positions of each one of the markers of the model, at
                    each time. This file is then used to determine the position of each
                    one of the joints of the model at each time.
            compl_ik_path : String.
                    Complete path to the Inverse Kinematics configuration file. 

        """
        if compl_ik_path != None and compl_ik_path != None:
            toolIK = osim.InverseKinematicsTool(compl_ik_path)
            toolIK.set_marker_file(trc_path)
            toolIK.setModel(self.osimModel)

        manager = osim.Manager(self.osimModel)
        manager.initialize(self.osimState)
        manager.integrate(time)    

         
    def displayModel(self):
        """!

        Method used to display the model          

        """
        viz = osim.VisualizerUtilities()
        viz.showModel(self.osimModel)

    def printJointsPositionsAllFrames(self ,refe = "Ground"):
        """!
        
        Method used to print the positions of all the frames of each joint, into a 
        chosen referential "refe".
        
        Args:
            refe : String, optional.
            
                Type of referential chosen to express the joints position. Two choices
                are available : 
                
                refe = "Ground", to express the positions into the global referential 
                Ground.
                
                refe = "Parent", to express the positions into the referential of the parent
                joint of the selected joint. This could be useful to determine the position 
                of the joint compared to the previous joint.
                    
                The default is "Ground".
        """
        print("--- Position of each joint referential in %s referential  ---"%refe)
        jointlist = self.osimModel.getJointList()
        for joint in jointlist:
            name = joint.getName()
            p_frame = joint.get_frames(0)
            if refe == "Ground":       
                p = p_frame.getTransformInGround(self.osimState).p()
                Q = p_frame.getTransformInGround(self.osimState).R().convertRotationToQuaternion()
            elif refe == "Parent":
                pp_frame = p_frame.getParentFrame()
                transf = p_frame.findTransformBetween(self.osimState,pp_frame)
                p = transf.p()
                Q = transf.R().convertRotationToQuaternion()                    
            M_q = self.numpy(Q)       
            r,pc,y = alg.euler_from_quaternion(M_q.flatten(),axes = 'sxyz')
            print("Name %s"%name)
            print("Parent Frame : {: .4f} {: .4f} {: .4f} | {: .4f} {: .4f} {: .4f} ".format(p.get(0), p.get(1), p.get(2), r,pc,y ) )
            c_frame = joint.get_frames(1)
            if refe == "Ground":       
                p = c_frame.getTransformInGround(self.osimState).p()
                Q = c_frame.getTransformInGround(self.osimState).R().convertRotationToQuaternion()
            elif refe == "Parent":
                pp_frame = c_frame.getParentFrame()
                transf = c_frame.findTransformBetween(self.osimState,pp_frame)
                p = transf.p()
                Q = transf.R().convertRotationToQuaternion()                    
            M_q = self.numpy(Q)       
            r,pc,y = alg.euler_from_quaternion(M_q.flatten(),axes = 'sxyz')       
            print("Child Frame  : {: .4f} {: .4f} {: .4f} | {: .4f} {: .4f} {: .4f} ".format(p.get(0), p.get(1), p.get(2), r,pc,y ) )

    def printJointsPositions(self, refe="Ground", n= 1):
        """!
        Method used to print the positions of one of the two frames of the joints
        of the model, into a chosen referential. Based on the method getJointsPositions.

        Args:
            refe : String, optional.
            
                Type of referential chosen to express the joints position. Two choices
                are available : 
                
                refe = "Ground", to express the positions into the global referential 
                Ground. 
                
                refe = "Parent", to express the positions into the referential of the parent
                joint of the selected joint. This could be useful to determine the position 
                of the joint compared to the previous joint.
                    
                The default is "Ground".
            
            n : Int, optional.
            
                Value associated with the joint frame to be selected for each of the 
                joints.
                Two frames are available : one frame fixed in relation to the parent joint, 
                the other fixed in relation to the current joint. 
                
                n = 0 : frame fixed in relation to the parent joint.
                
                n = 1 : frame fixed in relation to the current joint.
            
                The default is 1.

        """
        
        
        print("--- Position of each joint referential in %s referential  ---"%refe)
        jointlist = self.osimModel.getJointList()
        for joint in jointlist:
            name = joint.getName()
            p_frame = joint.get_frames(n)
            if refe == "Ground":
                p = p_frame.getTransformInGround(self.osimState).p()
                Q = p_frame.getTransformInGround(self.osimState).R().convertRotationToQuaternion()
            elif refe == "Parent":
                pp_frame = p_frame.getParentFrame()
                transf = p_frame.findTransformBetween(self.osimState, pp_frame)
                p = transf.p()
                Q = transf.R().convertRotationToQuaternion()           
            M_q = self.numpy(Q)       
            r,pc,y = alg.euler_from_quaternion(M_q.flatten(),axes = 'sxyz')
            print("Name {:<24} : {: .4f} {: .4f} {: .4f} | {: .4f} {: .4f} {: .4f} ".format(name, p.get(0), p.get(1), p.get(2), r,pc,y ) )

    def printBodiesPositions(self):
        """!
        Method used to get the positions of the body parts of the model into the
        ground referential.

        """
        
        print("--- Position of each referential associated with each body part in Ground referential  ---")
        bodylist = self.osimModel.getBodyList()
        for body in bodylist:
            name = body.getName()
            p = body.getTransformInGround(self.osimState).p()
            str_R = body.getTransformInGround(self.osimState).R().asMat33().toString()
            print("Name {:<24} : {: .4f} {: .4f} {: .4f} | ".format(name, p.get(0), p.get(1), p.get(2)), str_R)

    def getBodiesPositions(self):
        """!
        
        Method used to get the positions of the body parts of the model, expressed
        into the global referential Ground.

        Returns:
            D_body_pos : Dictionary of list.
            Dictionary expressing, for each body part, its position and rotation matrix
            into the Ground referential.
        
            Example : 
                    D_body_pos[hand_l] = [ M_p, M_R ]
                    M_p = np.array([[0.5],
                                    [0.5],
                                    [0.75]])
                    M_R = np.eye(3,3)

        """
        
        bodylist = self.osimModel.getBodyList()
        
        D_body_pos = {}
        
        for body in bodylist:
            name = body.getName()
            M_R = np.zeros((3,3))
            M_p = np.zeros((3,1))
            p = body.getTransformInGround(self.osimState).p()
            R = body.getTransformInGround(self.osimState).R().asMat33()
            M_R = self.numpy(R)
            M_p = self.numpy(p)

            D_body_pos[name] = [M_R,M_p]
        return(D_body_pos)
                
    def getJointsValues(self):
        """!
        
        Method used to get, for each coordinate object of the .osim model, its value.

        Returns:
            D_joint_value : Dictionary.
            Dictionary expressing, for each coordinate object, its value.
            Example : 
                    D_joint_value[pelvis_ty] = 2.0

        """
        coordinateSetlist = self.osimModel.getCoordinateSet()
        D_joint_value = {}    

        for coord in coordinateSetlist:
            name = coord.getName()
            D_joint_value[name] = coord.getValue(self.osimState)
        return(D_joint_value)

    def printFramesNumber(self):
        """!
        
        Method used to print the index and the name of each frame associated 
        with the model.

        """
        print("--- Number associated with each frame ---")
        framelist = self.osimModel.getFrameList()
        n = 0
        for frame in framelist:
            name = frame.getName()
            print("Name {:<24} : {: .4f}  ".format(name,n) )
            n+=1
        
    def printJointsValues(self):
        """!
        
        Method used to print the value of each of the coordinates of the model.

        """
        coordinateSetlist = self.osimModel.getCoordinateSet()
        print("--- Value of each articulation ---")
        for coord in coordinateSetlist:
            name = coord.getName()
            coordval = coord.getValue(self.osimState)
            print("Name {:<24} : {: .4f} ".format(name,coordval))

    def getMarkersByBodyParts(self):
        """!
        
        Method used to get dictionary of markers per body part

        Returns:
            D_body_markers: Dictionary of markers with body pars as keys

        """
        markerSet = self.osimModel.upd_MarkerSet()
        D_body_markers = {}
        for marker in markerSet:
            name = marker.getName() 
            phys_name = marker.getParentFrame().getName()
            frame_name = phys_name.split("/")[-1]
            if frame_name not in D_body_markers:
                D_body_markers[frame_name] = []
            D_body_markers[frame_name].append(name)


        # for i in range(markerSet.getSize()):
        #     name =osimModel.get_MarkerSet(i).getName()
        #     phys_name = osimModel.get_MarkerSet(i).getParentFrame().getName()
        #     frame_name = phys_name.split("/")[-1]
        #     if frame_name not in D_body_markers:
        #         D_body_markers[frame_name] = []

        return(D_body_markers)

    def getBodiesDistancesParentChild(self):
        """
            Method for calculating the distance in between parent and child

            Returns:
                D_body_index : indexes of bodies
                D_body_parent_child_distances : distance per body - body : [ [index_parent, dist], [ [index_child_i, dist_i] ]  ]
        """
        
        bodylist = self.osimModel.getBodyList()
        jointlist = self.osimModel.getJointList()
        D_body_parent_child_distances = {}        
        D_body_index = {}
        
        i = 0
        for body in bodylist:
            name = body.getName()
            
            D_body_parent_child_distances[name] = [ [], [] ]
            
            D_body_index[name] = i
            i+=1
        
        for joint in jointlist:

            M_R = np.zeros((3,3))
            M_p = np.zeros((3,1))
            
            # let's try to get informations about the frame 0
            
            frame_cons = joint.get_frames(0)
                
            p_frame = frame_cons.getParentFrame()
            
            # pp_frame = p_frame.getParentFrame()
            
            p_name = p_frame.getName()

            
            p_cons_transf = frame_cons.findTransformBetween(self.osimState,p_frame)
            
            p = p_cons_transf.p()
            R = p_cons_transf.R().asMat33()
            M_R = self.numpy(R)
            M_p = self.numpy(p)       
            p_dist = np.linalg.norm( np.dot(M_R,M_p) )
            
            try :
                p_index = D_body_index[p_name]
            except KeyError:
                p_index = -1
            
            frame_cons2 = joint.get_frames(1)
            c_frame = frame_cons2.getParentFrame()
            #c_frame = frame_cons2
            c_name = c_frame.getName()

            c_cons_transf = frame_cons2.findTransformBetween(self.osimState,c_frame)
            
            p = c_cons_transf.p()
            R = c_cons_transf.R().asMat33()
            M_R = self.numpy(R)
            M_p = self.numpy(p)       
            c_dist = np.linalg.norm( np.dot(M_R,M_p) )

            try :
                c_index = D_body_index[c_name]
            except KeyError:
                c_index = -1
            
            try :
                D_body_parent_child_distances[c_name][0] = [p_index,p_dist]
            except KeyError:
                pass
            try:
                D_body_parent_child_distances[p_name][1].append( [c_index,p_dist] )
            except KeyError:
                pass

        return(D_body_index,D_body_parent_child_distances)

    def fixScalingFactors(self, attribuedBodies):
        """
            Method for adapting scaling factors, (ask Erwann for info)
        """
        
        # nom joint -> pos par rapport au parent.
        D_jp = self.getJointsPositions("Parent",n=0)
        
        D_body_index,D_body_parent_child_distances = self.getBodiesDistancesParentChild()
        
        bodylist = self.osimModel.getBodyList()
        
        i = 0
        for body in bodylist:
            body_name = body.getName()
            
            if body_name not in attribuedBodies and body_name!="ground":
                L_concerned = D_body_parent_child_distances[body_name]
                
                min_dist = 9999
                min_index = 0
                pair_parent = L_concerned[0]
                
                if pair_parent[0] != -1:
                    min_dist = pair_parent[1]
                    min_index = pair_parent[0]
                
                for k in range(len(L_concerned[1])):
                    pair_child = L_concerned[1][k]
                    
                    if len(pair_child) >0 and pair_child[0] !=-1:
                        if min_dist > pair_child[1]:
                            min_dist = pair_child[1]
                            min_index = pair_child[0]
                body_chosen = body
                bodySet = self.osimModel.getBodySet()

                body_chosen = bodySet.get(min_index)
                
                scaleFactors = body_chosen.get_attached_geometry(0).get_scale_factors()
                print("old scale : ", self.osimModel.updBodySet().get(i).get_attached_geometry(0).get_scale_factors())
                
                self.osimModel.updBodySet().get(i).scale(scaleFactors)
                
                print("New scale factors : ", self.osimModel.updBodySet().get(i).get_attached_geometry(0).get_scale_factors())

            i+=1

    def reassembleByAxis(self, D_body_markers, parallal_val = 0.8):
        """
        For each body part, create (ask Erwann for info)

        """

        D_mark_pos = self.getMarkersPositionsInLocalReferential()
        #print(D_mark_pos)

        D_body_markerPair = {}

        bodyNames = []
        for body in D_body_markers:

            bodyNames.append(body)        
            list_mark = D_body_markers[body]
            
            L_body_marker_vector = []

            L_body_marker_pair = []

            for i in range(len(list_mark)):
                
                mark_i = list_mark[i]

                pos_i = D_mark_pos[mark_i]

                
                for j in range(i+1, len(list_mark)):

                    mark_j = list_mark[j]
                    
                    pos_j = D_mark_pos[mark_j]

                    n_vec = (pos_j-pos_i)/np.linalg.norm(pos_j-pos_i)

                    L_body_marker_vector.append(n_vec)

                    L_body_marker_pair.append([mark_i,list_mark[j]])
            
            # search composantes on X Y Z
            D_body_markerPair[body] = {'X':[], 'Y':[], 'Z':[] }
            #D_index = {'X':[], 'Y':[], 'Z':[]}
            L = ['X','Y','Z']

            L_max_vec = [L_body_marker_vector[0], L_body_marker_vector[0], L_body_marker_vector[0]]

            L_index = [0,0,0]

            #print("For body : ", body)

            for i in range(len(L_body_marker_vector) ):

                #print("For pair : ", L_body_marker_pair[i])

                vec = L_body_marker_vector[i]

                #print("Vector : ", vec)
                            
                for k in range(3):
                    compo = abs(vec[k])

                    if compo > parallal_val :
                        D_body_markerPair[body][L[k]].append(L_body_marker_pair[i])


                    if compo > abs(L_max_vec[k][k]):
                        L_max_vec[k] = vec
                        L_index[k] = i
            for k in range(3):
                if len(D_body_markerPair[body][L[k]]) == 0:
                    D_body_markerPair[body][L[k]].append(L_body_marker_pair[L_index[k]])
            
            #print("----------")

        return(D_body_markerPair,bodyNames)

    def scaling(self, dirPath, osimFile, scaleFile, trcFile, motFile, mass, scalingFactors):
        """
        Inspired from https://github.com/HernandezVincent/OpenSim (ask Erwann for info)
        """
        
        
        LosimPath = osimFile.split("/")
        
        osimPath = LosimPath[0]
        for i in range(1,len(LosimPath)-1):
            osimPath+= "/"+ LosimPath[i]
        
        osimPath+="/"
        
        model_name = LosimPath[-1].split(".")[0]
        D_body_markers = self.getMarkersByBodyParts()
        
        
        generic_MM_Path = osimFile                # Path to the Generic Musculoskeletal Model
        XML_generic_ST_path = scaleFile                            # Path to the generic Scale Tool XML file
        TRC_file = trcFile                                            # Path to the .trc file that contain the marker data
        
        XML_ST_file = dirPath + 'scaleToolSubject.xml'                             # Path to the subject Scale Tool XML file that will be created
        XML_SF_file = dirPath + 'scaleFactorSubject.xml'                           # Path to the subject Scale Factor file that will be created
        scaled_MM_path = dirPath + model_name+ 'Scaled.osim'                        # Path to the subject Musculoskeletal Model that will be created with the model scaler
        scaled_MM_path2 = osimPath + model_name + 'ScaledPlaced.osim'                  # Path to the subject Musculoskeletal Model that will be created with the marker placer

        markerData = osim.MarkerData(TRC_file)
        initial_time = markerData.getStartFrameTime()
        final_time = markerData.getLastFrameTime()
        TimeArray = osim.ArrayDouble()                                                 # Time range
        TimeArray.set(0,initial_time)
        TimeArray.set(1,final_time) 
        
        # Scale Tool
        scaleTool = osim.ScaleTool(XML_generic_ST_path)
        scaleTool.setName(model_name)                                                   # Name of the subject
        scaleTool.setSubjectMass(mass)                                                   # Mass of the subject
        scaleTool.setSubjectHeight(-1)                                                 # Only for information (not used by scaling)
        scaleTool.setSubjectAge(-1)                                                    # Only for information (not used by scaling)
        
        # Generic Model Maker
        scaleTool.getGenericModelMaker().setModelFileName(osimFile)
        #scaleTool.getGenericModelMaker().setMarkerSetFileName(XML_markers_path)
        
        # Model Scaler
        scaleTool.getModelScaler().setApply(True)

        scaleTool.getModelScaler().setMarkerFileName(TRC_file)                          
        scaleTool.getModelScaler().setTimeRange(TimeArray)
        scaleTool.getModelScaler().setPreserveMassDist(True)
        scaleTool.getModelScaler().setOutputModelFileName(scaled_MM_path)
        scaleTool.getModelScaler().setOutputScaleFileName(XML_SF_file)
        scaleTool.getModelScaler().setPreserveMassDist(True)
        
        # The scale factor information concern the pair of marker that will be used
        # to scale each body in your model to make it more specific to your subject.
        # The scale factor are computed with the distance the virtual markers and between your experimental markers
        

        
        
        measurementTemp = osim.Measurement()
        bodyScaleTemp = osim.BodyScale()
        markerPairTemp = osim.MarkerPair()
        scaleTemp = osim.Scale()
        
        bodyNames = []
        
        markerList = []

        for body in D_body_markers:
            markerList += D_body_markers[body]    

        
        isScalingFixed = ( len(scalingFactors) == len(D_body_markers) )
        
        print("Scaling is Fixed by user : ", isScalingFixed)
        
        if not isScalingFixed:
            scaleTool.getModelScaler().setScalingOrder(osim.ArrayStr('measurements', 1))        
            
            D_body_markerPair, bodyNames = self.reassembleByAxis(D_body_markers)
            #print("Pairs created : ", markerPairList)
            nBody = len(bodyNames)
        
            # Create a Marker Pair Set fo each body


            for i in range(0, nBody):
        
                print("For body : ", bodyNames[i])
                
                L = ['X','Y','Z']
                for k in range(3):
                    
                    measurement = measurementTemp.clone()
                    measurement.setName(bodyNames[i]+ '_'+ L[k]) # Whatever name you want(Usually I set the same name as the body)
                    # Create a Body Scale Set
                    bodyScale = bodyScaleTemp.clone()
                    bodyScale.setName(bodyNames[i]) # Name of the body
                    bodyScale.setAxisNames(osim.ArrayStr(L[k], 1))  # instead of 'X Y Z'
        
                    # Add body Scale set to Scaling
                    measurement.setApply(True)
                    measurement.getBodyScaleSet().adoptAndAppend(bodyScale)   
        
                    markerPairList = D_body_markerPair[bodyNames[i]][L[k]] 
        
                    print(" Axis %s : "%L[k], markerPairList)
        
                    for j in range(len(markerPairList)):
        
                        # Create a Marker Pair Set
                        markerPair = markerPairTemp.clone()
                        markerPair.setMarkerName(0, markerPairList[j][0])
                        markerPair.setMarkerName(1, markerPairList[j][1])
        
                        # Update the measurement to add the marker Pair
                
                        measurement.getMarkerPairSet().adoptAndAppend(markerPair)
        
                        # Add the measurement to the Model Scaler
                    scaleTool.getModelScaler().addMeasurement(measurement)
                    
        else:
            
            for body in D_body_markers:
                bodyNames.append(body)
            
            scaleTool.getModelScaler().setScalingOrder(osim.ArrayStr('manualScale', 1))

            for body in D_body_markers:
                
                scale = scaleTemp.clone()
                
                scale.setSegmentName(body)
                
                arD = osim.ArrayDouble(0.0,3)
                

                sc = scale.getScaleFactors()
                
                for i in range(3):
                    sc.set(i,scalingFactors[body][i])
                
                scale.setScaleFactors(sc)
                scale.setApply(True)
                
                scaleTool.getModelScaler().addScale(scale)

            
        # Create the subject Scale Tool XML file
        scaleTool.printToXML(XML_ST_file)
        print('Scale step : XML files : ' +  XML_ST_file + ' created')
        
        # Launch the scale tool again with the new XML file and then scale the
        # generic musculoskeletal model
        scaleTool = osim.ScaleTool(XML_ST_file)
        
        # Scale the model
        scaleTool.getModelScaler().processModel(self.osimModel)
        print('Scaled Musculoskeletal : ' + scaled_MM_path + ' created')
        
        # In this part, we will use the previous XML file created and update it
        # with the MarkerPlacer tool to Adjust the position of the marker on the
        # scaled musculoskeletal model
        
        # Load the scaled musculoskeletal model
        osimModelScaled = osim.Model(scaled_MM_path)
        osimStateScaled = osimModelScaled.initSystem()
        
        # Added : first, fix the scale factors that might be wrong
        # (ex : no markers on them)
        osimModelScaled,osimStateScaled = fixScalingFactors(osimModelScaled,osimStateScaled,bodyNames)
        
        # Launch the scale tool
        scaleTool = osim.ScaleTool(XML_ST_file)
        
        # Get the marker data from a.trc file
        # markerData = osim.MarkerData(TRC_file)
        # initial_time = markerData.getStartFrameTime()
        # final_time = markerData.getLastFrameTime()
        # TimeArray = osim.ArrayDouble() # Time range
        # TimeArray.set(0, initial_time)
        # TimeArray.set(1, final_time)
        
        # The static pose weights will be used to adjust the markers position in 
        # the model from a static pose. The weights of the markers depend of the
        # confidence you have on its position.In this example, all marker weight
        # are fixed to one.
        
        scaleTool.getMarkerPlacer().setApply(True) # Ajustement placement de marqueurs(true or false)
        scaleTool.getMarkerPlacer().setStaticPoseFileName(TRC_file) # trc files for adjustements(usually the same as static)
        scaleTool.getMarkerPlacer().setTimeRange(TimeArray) # Time range
        #scaleTool.getMarkerPlacer().setCoordinateFileName(motFile)
        scaleTool.getMarkerPlacer().setOutputModelFileName(scaled_MM_path2)
        scaleTool.getMarkerPlacer().setMaxMarkerMovement(0.0)
        
        measurementTemp = osim.Measurement()
        ikMarkerTaskTemp = osim.IKMarkerTask()
        
        for i in range(0, len(markerList) ):
        
            ikMarkerTask = ikMarkerTaskTemp.clone()
        
            ikMarkerTask.setName(markerList[i]) # Name of the markers
            ikMarkerTask.setApply(True)
            ikMarkerTask.setWeight(1.0)
        
            scaleTool.getMarkerPlacer().getIKTaskSet().adoptAndAppend(ikMarkerTask)
        
        # Create the subject Scale Tool XML file
        scaleTool.printToXML(XML_ST_file)
        print('Placer step : XML files : ' + XML_ST_file + ' created')
        
        # Launch the ScaleTool again
        scaleTool = osim.ScaleTool(XML_ST_file)
        scaleTool.getMarkerPlacer().processModel(osimModelScaled) #processModel(osimStateScaled, osimModelScaled)
        print('Adjusted markers on the musculoskeletal done')
        
    def ikFromFile(self, trc_path, compl_ik_path,mot_path):
        """!

        Method calculating inverse kinematics from file

        Args:
            trc_path : String
                    Path to a trc file associated with the .osim file. This file contains
                    the different positions of each one of the markers of the model, at
                    each time. This file is then used to determine the position of each
                    one of the joints of the model at each time.
            compl_ik_path : String.
                    Complete path to the Inverse Kinematics configuration file. 
            mot_path : String
                Path to the output motion file of Inverse Kinematics.


        """
        path, filename = os.path.split(trc_path)
        filename, ext = os.path.splitext(filename)

        # Marker Data

        markerData = osim.MarkerData(trc_path)

        # Set the IK tool
        ikTool = osim.InverseKinematicsTool(compl_ik_path)
        ikTool.setModel(self.osimModel)
        ikTool.setName(filename + ext)
        ikTool.setMarkerDataFileName(trc_path)


        ikTool.setOutputMotionFileName(mot_path)

        ikTool.run()
        
    def IK_from_markers_positions(self, markers_positions,xml_taskset,trc_path):
        """
        Method calculating inverse kinematics from marker positions (ask Erwann for info)
        """
        
        # voir IK from file de pyosim
        s = self.osimModel.initSystem()
        markers_ref = osim.MarkersReference()
        
        aweights = markers_ref.getMarkerWeightSet()
        
        
        ikTool = osim.InverseKinematicsTool(xml_taskset)
        
        IKTaskSet = ikTool.getIKTaskSet()
        
        #IKTaskSet = osim.IKTaskSet(xml_taskset)
        #print(IKTaskSet.getWeight())
        
        IKTaskSet.createMarkerWeightSet(aweights)
        
        print("size : " ,aweights.getSize())
        print("num : " ,aweights.get(0).getWeight())
        
        markerNames = []
        
        L = osim.ArrayStr()
        aweights.getNames(L)
        
        
        
        for i in range(L.getSize()):
            markerNames.append(L.get(i))
        
        
        M = osim.MatrixVec3(1,len(markerNames))
        for i in range(len(markerNames)):
            V = osim.Vec3()
        
            V.set(0,markers_positions[3*i])
            V.set(1,markers_positions[3*i+1])
            V.set(2,markers_positions[3*i+2])
        
            M.set(0,i,V)
            
        
        #table2 = osim.TimeSeriesTable([0.0],M,label_col)


        table = osim.TimeSeriesTableVec3([0.0],M,markerNames)
        
        # print(table)
        # print(type(table))
        
        nmarker_ref = osim.MarkersReference(table,aweights)
        
        
        coord_ref = osim.SimTKArrayCoordinateReference(len(markerNames))
        
        #coord_ref.getConcreteClassName()
        obj = nmarker_ref
        
        dwn = nmarker_ref.safeDownCast(obj)
        print(nmarker_ref)
        
        print(dwn)
        
        #IKSolver = osim.InverseKinematicsSolver(osimModel,nmarker_ref.safeDownCast(obj),coord_ref)
        
        #markers_ref.setMarkerWeightSet(aweights)
        
        
        
        # tst_m_ref = osim.MarkersReference(trc_path,aweights)
        
        # table = tst_m_ref.getMarkerTable()
        # #print(table)
        
        # #M = osim.simTK
        # M = osim.Matrix(1,2)
        # M.set(0,0, 1.0)
        # M.set(0,1, 1.0)
        # table2 = osim.TimeSeriesTable([0.0],M,["name1", "name2"])
        # print("\nTable2")
        # print(table2)
        
        # marker reference :
        

        
        # InverseKinematicsSolver (const Model &model, 
        #MarkersReference &markersReference, 
        #SimTK::Array_< CoordinateReference > &coordinateReferences, 
        # double constraintWeight=SimTK::Infinity)
        
    def getMarkersPositions(self):
        markerSet = self.osimModel.getMarkerSet()
        D_markers = {}
        for marker in markerSet:
            name = marker.getName()
            position = self.numpy(marker.getLocationInGround(self.osimState) )
            D_markers[name] = position
        return(D_markers)

    def getMarkersPositionsInLocalReferential(self):
        markerSet = self.osimModel.getMarkerSet()
        D_markers = {}
        for marker in markerSet:
            name = marker.getName()
            position = self.self.numpy(marker.get_location() )
            D_markers[name] = position
        return(D_markers)    

    def printMarkersPositions(self, nameToo = False):
        D_markers = self.getMarkersPositions()
        first = True

        for name in D_markers:
            pos = D_markers[name]
            if nameToo == False:
                if first : 
                    print("{ "," {: .6f} ,  {: .6f} ,  {: .6f} ,".format(pos[0,0],pos[1,0],pos[2,0]) )
                    first = False
                else:
                    print("{: .6f} ,  {: .6f} ,  {: .6f} ,".format(pos[0,0],pos[1,0],pos[2,0]) )
            
            else:
                print("Name : {<24} | Position : [{: .4f} ,  {: .4f} ,  {: .4f}  ]".format(name,pos[0,0],pos[1,0],pos[2,0]))
        
        if nameToo == False:
            print(" };")
        
    def getJacobian(self, v=[0,0,0], n=0):
        """!
        
        Return a 6xNQ frame task Jacobian, for a frame task A fixed to a body B
        designated by an) index n. 
        NQ is the size of the vector Q. The origin of the frame task A into the
        referential B is expressed by the vector v.

        Args:
            v : opensim.simbody.Vec3, optional.
                Origin of the frame task A into the referential B. The default is osim.Vec3().
                
            n : Int, optional.
                Mobilized Body Index. The body index to which the frame of interest
                is fixed. The default is 0

        """

        smss = self.osimModel.getMatterSubsystem()
        J0 = osim.Matrix()

        # obtain Jacobian
        smss.calcFrameJacobian(self.osimState, n, osim.Vec3(v), J0)
        # smss.calcSystemJacobian(self.osimState, J0)
        # J0 = J0[n*6:(n+1)*6,:]
        J0 = self.numpy(J0)
        return J0

    def tryForwardTool(self):
        """!
        
        (BETA). Method used to try the Forward Tool of Opensim. (ask Erwann for info)

        Args:
            osimModel : opensim.simulation.Model. 
                Object describing a .osim model, from the opensim module.

        
        """
        self.osimModel.initSystem()

        forward = osim.ForwardTool()
        forward.setManager()
        forward.setModel(osimModel)
        forward.setName('test_forward')
        forward.setFinalTime(0.05)
        forward.run()

    def __setQVals(self,LQ):
        """!
        
        Method used to set the Q vector of a .osim Model, according to a list
        LQ.    

        Args:
            osimModel : opensim.simulation.Model. 
                Object describing a .osim model, from the opensim module.

            osimState : opensim.simbody.State.
                Object describing the state of a .osim model, from the opensim module.

            LQ : List.
                List containing the values of each joint of the model.
                
        """
        sWorkingCopy = self.osimModel.updWorkingState()
        sWorkingCopy.setTime(self.osimState.getTime())
        self.osimModel.initStateWithoutRecreatingSystem(sWorkingCopy)

        Q = self.osimState.getQ()
        for k in range(len(LQ)):
            Q[k] = LQ[k]

        sWorkingCopy.setQ(Q)
        self.osimModel.realizePosition(sWorkingCopy)    

    def setToNeutral(self):
        """!
        
        Method used to set to neutral value all of the values of the articulations of the system.

        """

        coordSet = self.osimModel.updCoordinateSet()

        for k in range(coordSet.getSize()):
            coord = self.osimModel.updCoordinateSet().get(k)
            default = coord.getDefaultValue()
            coord.setValue(self.osimState,default,False)
        
        self.osimModel.assemble(self.osimState)

    def setToRandom(self):
        """!
        
        Method used to set to random values all of the values of the articulations of the system.

        """
        coordSet = self.osimModel.updCoordinateSet()

        for coord in coordSet:
            if not coord.isConstrained(self.osimState): # update only not constrained variables
                min_r = coord.get_range(0)
                max_r = coord.get_range(1)
                rand = (max_r - min_r )*random.random() + min_r
                coord.setValue(self.osimState, rand, False)
        
        self.osimModel.assemble(self.osimState)

    def setJointValues(self ,q):
        """!
        
        Method used to set to random values all of the values of the articulations of the system.

        Args: 
            q : list
                List of joint values, only the unconstrained joints

        """
        coordSet = self.osimModel.getCoordinateSet()
        i = 0
        for coord in coordSet:
            if not coord.isConstrained(self.osimState): # update only not constrained variables
                q[i] = np.clip(q[i], coord.get_range(0),coord.get_range(1))
                coord.setValue(self.osimState, q[i], True)
                i = i + 1
        
        self.osimModel.assemble(self.osimState)

    def getMomentArm(self, indDOF=None, indMuscles=None):
        """
        Moment arm matrix calculation (translated from Matlab code created by Nasser)

        Args:
            indDOF: indexes of used joints (DOF) of the model (by default all used)
            indMuscles: indexes ofmuslces of the model (by default all used) 

        Returns:
            MomentArm: matrix of indDOF x indMuscles elements 
        """


        # all the degrees of freedom
        coordSet = self.osimModel.getCoordinateSet()
        # all the muscles
        muscleSet = self.osimModel.getMuscles()

        # indexes of the dof used
        if not indDOF:
            indDOF = range(coordSet.getSize())

        # indexes of the muscles used
        if not indMuscles:
            indMuscles = range(muscleSet.getSize()) 

        # number of used dof and mucsles
        nMuscles = len(indMuscles)
        nDOF     = len(indDOF)

        
        # Make sure the muscles states are in equilibrium
        self.osimModel.equilibrateMuscles(self.osimState)

        # Init the moment arm matrix 
        MomentArm = np.zeros((nDOF, nMuscles))
        for i, iDOF in enumerate(indDOF):
            # Get the object coresponding to the dof with the index iDOF
            coord = coordSet.get(iDOF)

            for j, iMuscle in enumerate(indMuscles):
                # get the muscle object with the index iMuscle
                muscle = muscleSet.get(iMuscle) ##ok<*IJCL>
                # Calculate the momnet arm matrix entry 
                #print( muscle.computeMomentArms(self.osimState))
                MomentArm[i,j] = muscle.computeMomentArm(self.osimState, coord)#Calcul du bras de levier    

        return MomentArm

    def getForceLimits(self):
        """!
        
        Method returning the current maximal force limits for each muscle. Upper limit is the max 
        isometric force and lower limit is the passive force applied

        Returns
            F_min: list
                List of minimal forces

            F_max: list
                List of maximal forces

        """
        F_min = []
        F_max = []
        for muscle in self.osimModel.getMuscles():
            F_max.append(muscle.getMaxIsometricForce()) 
            F_min.append(muscle.getPassiveFiberForce(self.osimState)) 
            if F_min[-1] > F_max[-1]:
                F_min[-1] = 0.1*F_max[-1]

        return F_min, F_max


    def numpy(self, matrix):
        """!
        
        Method used to transform OpenSim Mat33 or Vec into numpy objects.

        Args:
            matrix : opensim.simbody.Mat33 or opensim.simbody.Vec.
                Input Opensim matrix.
        Returns:
            M : numpy.array.
                Output matrix, as array.

        """
        mat = True
        mat_type = None
        
        try:
            matrix.get(0,0)
        except Exception:
            mat = False

        if mat:
            mat_type = type(matrix.get(0,0))
            M = self.__Mnumpy(matrix,mat_type)
        else:
            mat_type = type(matrix.get(0))
            M = self.__Vnumpy(matrix,mat_type)
        return(M)

    def __Vnumpy(self, matrix, mat_type):
        """!
        
        Transform a Vec object into a numpy array.

        Args:
            matrix : opensim.simbody.Vec.
                Opensim Vector
            mat_type : type.
                Type of the vector (String, float, int, ...)
        Returns:
            V : numpy.array.
                Vector array.

        """
        nrow = matrix.size()
        V= np.zeros((nrow,1)).astype(mat_type)
        for i in range(nrow):
            V[i,0] = matrix.get(i)
        return(V)

    def __Mnumpy(self, matrix, mat_type):
        """!
        
        Transform a Mat33 object into a numpy array.

        Args:
            matrix : opensim.simbody.Mat33.    Opensim Mat33
            mat_type : type.  Type of the vector (String, float, int, ...)
        Retuns:
            M : numpy.array. Matrix array.

        """    
        ncol = matrix.ncol()
        nrow = matrix.nrow()
        M = np.zeros((nrow,ncol)).astype(mat_type)
        
        for i in range(nrow):
            for j in range(ncol):
                M[i,j] = matrix.get(i,j)
        return(M)



if __name__ == "__main__":
    
    ## Constructor of the OsimModel class.
    model = OsimModel(model_path="./arm26.osim", geometry_path="./Geometry", visualize=True)

    time.sleep(0.1)
    # set model to neutral or random state
    model.setToRandom()
    # model.setToNeutral()

    #print different model variables
    model.printBodiesPositions()
    model.printMarkersPositions()
    model.printJointsValues()
    
    # print model jacobian
    print(model.getJacobian([0,0,0],n=2))

    # display model
    model.displayModel()
