<!-- markdownlint-disable -->

# <kbd>module</kbd> `pyosim.OsimModel`
Created on Wed Dec 16 13:31:14 2020 

@author: elandais 



---

## <kbd>class</kbd> `OsimModel`
! Class creating a Osim Model, saving and updating useful informations about it. 

### <kbd>method</kbd> `__init__`

```python
__init__(
    model_path,
    geometry_path='',
    ik_path='',
    setup_ik_file='',
    trc_path='',
    visualize=False
)
```

! 

Constructor of the OsimModel class. 

@param model_path : String.  Absolute path to the .osim file (with the .osim filename).  

@param geometry_path : String.  Path to the .vtp files associated with the .osim model.  

@param ik_path : String, optional.  Path to the Inverse Kinematics configuration file. The default is "".  

@param setup_ik_file : String, optional.  Name of the Inverse Kinematics configuration file. This file is used  to set parameters of the Inverse Kinematics operation (weight of each  markers of the model for example).  The default is "".  

@param trc_path : String, optional.  Path to a trc file associated with the .osim file. This file contains  the different positions of each one of the markers of the model, at  each time. This file is then used to determine the position of each  one of the joints of the model at each time.  The default is "".         

@param visualize : Boolean, optional  Activate the visualization of the model. The default is False. 

@return None. 




---

### <kbd>method</kbd> `IK_from_markers_positions`

```python
IK_from_markers_positions(markers_positions, xml_taskset, trc_path)
```





---

### <kbd>method</kbd> `displayModel`

```python
displayModel(trc_path=None, compl_ik_path=None)
```

! 

(BETA). Method used to display a .osim Model.             

@param osimModel : opensim.simulation.Model.   Object describing a .osim model, from the opensim module. 

@param osimState : opensim.simbody.State.  Object describing the state of a .osim model, from the opensim module.  

@param trc_path : String  Path to a trc file associated with the .osim file. This file contains  the different positions of each one of the markers of the model, at  each time. This file is then used to determine the position of each  one of the joints of the model at each time. 

 

@param compl_ik_path : String.  Complete path to the Inverse Kinematics configuration file.  

@return None. 

---

### <kbd>method</kbd> `fixScalingFactors`

```python
fixScalingFactors(attribuedBodies)
```





---

### <kbd>method</kbd> `getBodiesDistancesParentChild`

```python
getBodiesDistancesParentChild()
```

body : [ [index_parent, dist], [ [index_child_i, dist_i] ]  ] 

---

### <kbd>method</kbd> `getBodiesPositions`

```python
getBodiesPositions()
```

! 

Method used to get the positions of the body parts of the model, expressed into the global referential Ground. 



@return D_body_pos : Dictionary of list. Dictionary expressing, for each body part, its position and rotation matrix into the Ground referential. 

Example :   D_body_pos[hand_l] = [ M_p, M_R ]  M_p = np.array([[0.5],  [0.5],  [0.75]])  M_R = np.eye(3,3) 

---

### <kbd>method</kbd> `getCorresJointsCoord`

```python
getCorresJointsCoord()
```

! 

Method used to get a quick access to the correspondances between the joints of the model and the coordinates associated with each one of those joints. 

A coordinate is associated to an unique joint, but a joint can have multiple coordinates.  

@return D_coord_joints : Dictionary.  Dictionary linking the names of the coordinates objects to the name of  their respective joints.  Ex : D_coord_joints[pelvis_tx] = "pelvis"  D_coord_joints[pelvis_ty] = "pelvis"  D_coord_joints[pelvis_tz] = "pelvis"  D_coord_joints[pelvis_rx] = "pelvis"  D_coord_joints[pelvis_ry] = "pelvis"  D_coord_joints[pelvis_rz] = "pelvis" 



@return D_joints_coord : Dictionary.   Dictionary linking the names of the joints to the names of the coordinates  objects associated with these joints.   Ex : D_joints_coord[pelvis] = [pelvis_tx,pelvis_ty,pelvis_tz,pelvis_rx,pelvis_ry,pelvis_rz] 

---

### <kbd>method</kbd> `getCorresQCoord`

```python
getCorresQCoord()
```

! 

Method used to get the correspondance between the index associated with the value of the joints of the model, for the variable coordinateSet  and the variable Q. 

@return D_Q_coord : Dictionary.     D_Q_coord is organised as follows :   D_Q_coord[i] = j <--->  Q[i] = coordinateSet[j].getValue(osimState).  

 The value of coordinate j (wrt CoordinateSet) corresponds to the i-th component  of Q from osimState. 

---

### <kbd>method</kbd> `getForceLimits`

```python
getForceLimits()
```

! 

Method returning the current maximal force limits for each muscle. Upper limit is the max  isometric force and lower limit is the passive force applied 

@return F_min: list  List of minimal forces 

@return F_max: list  List of maximal forces 

---

### <kbd>method</kbd> `getGroundProperties`

```python
getGroundProperties()
```

! 

Method used to gather some useful properties about the Ground link of the .osim model.  

@return D_gd_prop : Dictionary of list.  Dictionary containing, for each of the coordinates (= DOF) names of the   Ground joint, the type and the axis associated with this coordinate. 

---

### <kbd>method</kbd> `getJacobian`

```python
getJacobian(v=[0, 0, 0], n=0)
```

! 

Return a 6xNQ frame task Jacobian, for a frame task A fixed to a body B designated by an) index n.  NQ is the size of the vector Q. The origin of the frame task A into the referential B is expressed by the vector v. 

@param v : opensim.simbody.Vec3, optional.  Origin of the frame task A into the referential B. The default is osim.Vec3().  

@param n : Int, optional.  Mobilized Body Index. The body index to which the frame of interest  is fixed. The default is 0. 

@return None. 

---

### <kbd>method</kbd> `getJointsLimits`

```python
getJointsLimits()
```

! 

Method used to get the limits values of each one of the DOF of the model. 

@return D_joints_range : Dictionary of List  D_joints_range is organised as follows :   D_joints_range[i] = [lower limit of DOF i, upper limit of DOF i]. 

---

### <kbd>method</kbd> `getJointsPositions`

```python
getJointsPositions(refe='Ground', n=1)
```

! Method used to get the positions of one of the two frames of the joints of the model. The positions of the selected frames can then be expressed into  the global referential "Ground" of the model, or the referential of the parent joint "Parent" of the selected joint. 

@param refe : String, optional. 

 Type of referential chosen to express the joints position. Two choices  are available :   

 refe = "Ground", to express the positions into the global referential   Ground.   

 refe = "Parent", to express the positions into the referential of the parent  joint of the selected joint. This could be useful to determine the position   of the joint compared to the previous joint.  

 The default is "Ground". 

@param n : Int, optional. 

 Value associated with the joint frame to be selected for each of the   joints.  Two frames are available : one frame fixed in relation to the parent joint,   the other fixed in relation to the current joint.   

 n = 0 : frame fixed in relation to the parent joint.  

 n = 1 : frame fixed in relation to the current joint. 

 The default is 1. 

@return D_joints_pos : Dictionary of list.  Dictionary expressing, for each joint, its position and rotation matrix   in the chosen referential.   

 Example :   D_joints_pos[pelvis] = [ M_p, M_R ]  M_p = np.array([[0.],  [2.],  [0.]])  M_R = np.eye(3,3) 

---

### <kbd>method</kbd> `getJointsValues`

```python
getJointsValues()
```

! 

Method used to get, for each coordinate object of the .osim model, its value. 

@return D_joint_value : Dictionary. Dictionary expressing, for each coordinate object, its value. Example :   D_joint_value[pelvis_ty] = 2.0 

---

### <kbd>method</kbd> `getMarkersByBodyParts`

```python
getMarkersByBodyParts()
```





---

### <kbd>method</kbd> `getMarkersPositions`

```python
getMarkersPositions()
```





---

### <kbd>method</kbd> `getMarkersPositionsInLocalReferential`

```python
getMarkersPositionsInLocalReferential()
```





---

### <kbd>method</kbd> `getMomentArm`

```python
getMomentArm(indDOF=None, indMuscles=None)
```





---

### <kbd>method</kbd> `ikFromFile`

```python
ikFromFile(trc_path, compl_ik_path, mot_path)
```

! 



@param trc_path : String  Path to a trc file associated with the .osim file. This file contains  the different positions of each one of the markers of the model, at  each time. This file is then used to determine the position of each  one of the joints of the model at each time. 

 

@param compl_ik_path : String.  Complete path to the Inverse Kinematics configuration file.  

@param mot_path : String  Path to the output motion file of Inverse Kinematics. 

@return None. 

---

### <kbd>method</kbd> `numpy`

```python
numpy(matrix)
```

! 

Method used to transform OpenSim Mat33 or Vec into numpy objects. 

@param matrix : opensim.simbody.Mat33 or opensim.simbody.Vec.  Input Opensim matrix. 

@return M : numpy.array.  Output matrix, as array. 

---

### <kbd>method</kbd> `printBodiesPositions`

```python
printBodiesPositions()
```

! Method used to get the positions of the body parts of the model into the ground referential. 

@return None. 

---

### <kbd>method</kbd> `printCorresQCoord`

```python
printCorresQCoord()
```

! 

Method used to print the correspondance between the index associated with the value of the joints of the model, for the variable coordinateSet  and the variable Q. This method is based on the method getCorresQCoord. 

@return None. 

---

### <kbd>method</kbd> `printFramesNumber`

```python
printFramesNumber()
```

! 

Method used to print the index and the name of each frame associated  with the model. 

@return None. 

---

### <kbd>method</kbd> `printJointsDivisionsIntoOneDOF`

```python
printJointsDivisionsIntoOneDOF()
```

! 

Method used to print the correspondances between the joints of the model and the coordinates associated with each one of those joints. Based on the method getCorresJointsCoord. 

@return None. 

---

### <kbd>method</kbd> `printJointsLimits`

```python
printJointsLimits()
```

! 

Method used to print the limits values of each one of the DOF of the model, based on the method getJointsLimits 

---

### <kbd>method</kbd> `printJointsPositions`

```python
printJointsPositions(refe='Ground', n=1)
```

! Method used to print the positions of one of the two frames of the joints of the model, into a chosen referential. Based on the method getJointsPositions. 

@param refe : String, optional. 

 Type of referential chosen to express the joints position. Two choices  are available :   

 refe = "Ground", to express the positions into the global referential   Ground.   

 refe = "Parent", to express the positions into the referential of the parent  joint of the selected joint. This could be useful to determine the position   of the joint compared to the previous joint.  

 The default is "Ground". 

@param n : Int, optional. 

 Value associated with the joint frame to be selected for each of the   joints.  Two frames are available : one frame fixed in relation to the parent joint,   the other fixed in relation to the current joint.   

 n = 0 : frame fixed in relation to the parent joint.  

 n = 1 : frame fixed in relation to the current joint. 

 The default is 1. 

@return None. 

---

### <kbd>method</kbd> `printJointsPositionsAllFrames`

```python
printJointsPositionsAllFrames(refe='Ground')
```

! 

Method used to print the positions of all the frames of each joint, into a  chosen referential "refe".  

@param refe : String, optional. 

 Type of referential chosen to express the joints position. Two choices  are available :   

 refe = "Ground", to express the positions into the global referential   Ground.  

 refe = "Parent", to express the positions into the referential of the parent  joint of the selected joint. This could be useful to determine the position   of the joint compared to the previous joint.  

 The default is "Ground". 

@return None. 

---

### <kbd>method</kbd> `printJointsValues`

```python
printJointsValues()
```

! 

Method used to print the value of each of the coordinates of the model. 



@return None. 

---

### <kbd>method</kbd> `printMarkersPositions`

```python
printMarkersPositions(nameToo=False)
```





---

### <kbd>method</kbd> `reassembleByAxis`

```python
reassembleByAxis(D_body_markers, parallal_val=0.8)
```

For each body part, create  

---

### <kbd>method</kbd> `runIK`

```python
runIK()
```

! 

Method used to execute the Inverse Kinematics operation. The variables  motion_file (=path to the output motion file of Inverse Kinematics),  compl_ik_path (= path to the Inverse Kinematics configuration file) and  trc_path (= path to the marker file used by Inverse Kinematics) should  be defined first.  Those variables are defined at the constructor step if the variables ik_path, setup_ik_file and trc_path were defined. 

@return None. 

---

### <kbd>method</kbd> `scaling`

```python
scaling(dirPath, osimFile, scaleFile, trcFile, motFile, mass, scalingFactors)
```

Inspired from https://github.com/HernandezVincent/OpenSim 

---

### <kbd>method</kbd> `setJointValues`

```python
setJointValues(q)
```

! 

Method used to set to random values all of the values of the articulations of the system. 

@param q : list  List of joint values, only the unconstrained joints 

@return None.opensim  

---

### <kbd>method</kbd> `setQ`

```python
setQ(LQ, LQtype=0)
```

! 

Set the Q vector of the .osim model, containing the values of each joint of this model. Those values are set in accordance to a list of variables LQ.  

@param LQ : List. 

 List containing the values of each joint of the model.  

@param LQtype : Int. 

 Define the configuration of the LQ list.  

 Type 0 : Configuration according to the order of the CoordinateSet  variable of the .osim Model.  LQ[i] corresponds to the desired value of CoordinateSet[i]. As there  is no direct link between CoordinateSet[i] and Q[i], a transformation is  necessary.  

 Type 1 : Configuration according to the order of Q vector used by   osimState.  LQ[i] corresponds to the desired value of Q[i] used by osimState.   Here, no transformation is necessary.              

 The default is 0. 

@return None. 

---

### <kbd>method</kbd> `setQVals`

```python
setQVals(LQ)
```

! 

Method used to set the Q vector of a .osim Model, according to a list LQ.     

@param osimModel : opensim.simulation.Model.   Object describing a .osim model, from the opensim module. 

@param osimState : opensim.simbody.State.  Object describing the state of a .osim model, from the opensim module. 

@param LQ : List.  List containing the values of each joint of the model.  



@return None. 

---

### <kbd>method</kbd> `setToNeutral`

```python
setToNeutral()
```

! 

Method used to set to neutral value all of the values of the articulations of the system. 

@return None. 

---

### <kbd>method</kbd> `setToRandom`

```python
setToRandom()
```

! 

Method used to set to random values all of the values of the articulations of the system. 

@return None. 

---

### <kbd>method</kbd> `tryForwardTool`

```python
tryForwardTool()
```

! 

(BETA). Method used to try the Forward Tool of Opensim. 

@param osimModel : opensim.simulation.Model.   Object describing a .osim model, from the opensim module. 

@return None. 

---

### <kbd>method</kbd> `updModel`

```python
updModel()
```

! 

Method used to update the different variables of the OsimModel class. This method should be called after each operation on the state of the .osim model. 

@return None. 




---

_This file was automatically generated via [lazydocs](https://github.com/ml-tooling/lazydocs)._
