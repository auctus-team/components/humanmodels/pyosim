<!-- markdownlint-disable -->

# API Overview

## Modules

- [`OsimModel`](./OsimModel.md#module-osimmodel): Created on Wed Dec 16 13:31:14 2020
- [`algebra`](./algebra.md#module-algebra)

## Classes

- [`OsimModel.OsimModel`](./OsimModel.md#class-osimmodel): !

## Functions

- [`algebra.angle`](./algebra.md#function-angle)
- [`algebra.dotproduct`](./algebra.md#function-dotproduct)
- [`algebra.euler_from_matrix`](./algebra.md#function-euler_from_matrix): Return Euler angles from rotation matrix for specified axis sequence.
- [`algebra.euler_from_quaternion`](./algebra.md#function-euler_from_quaternion): Return Euler angles from quaternion for specified axis sequence.
- [`algebra.euler_from_rot_mat`](./algebra.md#function-euler_from_rot_mat): Euler angles from rotation matrix ( 3x3 )
- [`algebra.euler_matrix`](./algebra.md#function-euler_matrix): Return homogeneous rotation matrix from Euler angles and axis sequence.
- [`algebra.inv_rpytoQUAT`](./algebra.md#function-inv_rpytoquat): Takes euler angles and return a quaternion
- [`algebra.length`](./algebra.md#function-length)
- [`algebra.nullSpace`](./algebra.md#function-nullspace): Computes the null space from SVD
- [`algebra.q_mult`](./algebra.md#function-q_mult)
- [`algebra.quaternion_about_axis`](./algebra.md#function-quaternion_about_axis): Return quaternion for rotation about axis.
- [`algebra.quaternion_from_matrix`](./algebra.md#function-quaternion_from_matrix): Return quaternion from rotation matrix.
- [`algebra.quaternion_matrix`](./algebra.md#function-quaternion_matrix): Return homogeneous rotation matrix from quaternion.
- [`algebra.rotation_from_matrix`](./algebra.md#function-rotation_from_matrix): Return rotation angle and axis from rotation matrix.
- [`algebra.rotation_matrix`](./algebra.md#function-rotation_matrix): Return the rotation matrix associated with counterclockwise rotation about
- [`algebra.rpytoQUAT`](./algebra.md#function-rpytoquat)
- [`algebra.svdDecomposition`](./algebra.md#function-svddecomposition)
- [`algebra.svdDecompositionBis`](./algebra.md#function-svddecompositionbis)


---

_This file was automatically generated via [lazydocs](https://github.com/ml-tooling/lazydocs)._
