<!-- markdownlint-disable -->

# <kbd>module</kbd> `pyosim.algebra`





---

## <kbd>function</kbd> `nullSpace`

```python
nullSpace(A, eps=1e-15)
```

Computes the null space from SVD returns the nulls space and the rank 


---

## <kbd>function</kbd> `svdDecomposition`

```python
svdDecomposition(A, threshold_value)
```






---

## <kbd>function</kbd> `svdDecompositionBis`

```python
svdDecompositionBis(A, M, threshold_value)
```






---

## <kbd>function</kbd> `dotproduct`

```python
dotproduct(v1, v2)
```






---

## <kbd>function</kbd> `length`

```python
length(v)
```






---

## <kbd>function</kbd> `angle`

```python
angle(v1, v2)
```






---

## <kbd>function</kbd> `euler_matrix`

```python
euler_matrix(ai, aj, ak, axes='sxyz')
```

Return homogeneous rotation matrix from Euler angles and axis sequence. 

ai, aj, ak : Euler's roll, pitch and yaw angles axes : One of 24 axis sequences as string or encoded tuple 

``` R = euler_matrix(1, 2, 3, 'syxz')```
``` np.allclose(np.sum(R[0]), -1.34786452)``` True ``` R = euler_matrix(1, 2, 3, (0, 1, 0, 1))```
``` np.allclose(np.sum(R[0]), -0.383436184)``` True ``` ai, aj, ak = (4*math.pi) * (np.random.random(3) - 0.5)```
``` for axes in _AXES2TUPLE.keys():``` ...    R = euler_matrix(ai, aj, ak, axes) ``` for axes in _TUPLE2AXES.keys():```
...    R = euler_matrix(ai, aj, ak, axes)



---

## <kbd>function</kbd> `quaternion_matrix`

```python
quaternion_matrix(quaternion)
```

Return homogeneous rotation matrix from quaternion. 

``` M = quaternion_matrix([0.99810947, 0.06146124, 0, 0])```
``` np.allclose(M, rotation_matrix(0.123, [1, 0, 0]))``` True ``` M = quaternion_matrix([1, 0, 0, 0])```
``` np.allclose(M, np.identity(4))``` True ``` M = quaternion_matrix([0, 1, 0, 0])```
``` np.allclose(M, np.diag([1, -1, -1, 1]))``` True 


---

## <kbd>function</kbd> `quaternion_from_matrix`

```python
quaternion_from_matrix(matrix, isprecise=False)
```

Return quaternion from rotation matrix. 

If isprecise is True, the input matrix is assumed to be a precise rotation matrix and a faster algorithm is used. 

``` q = quaternion_from_matrix(np.identity(4), True)```
``` np.allclose(q, [1, 0, 0, 0])``` True ``` q = quaternion_from_matrix(np.diag([1, -1, -1, 1]))```
``` np.allclose(q, [0, 1, 0, 0]) or np.allclose(q, [0, -1, 0, 0])``` True ``` R = rotation_matrix(0.123, (1, 2, 3))```
``` q = quaternion_from_matrix(R, True)``` ``` np.allclose(q, [0.9981095, 0.0164262, 0.0328524, 0.0492786])```
True
``` R = [[-0.545, 0.797, 0.260, 0], [0.733, 0.603, -0.313, 0],``` ...      [-0.407, 0.021, -0.913, 0], [0, 0, 0, 1]] ``` q = quaternion_from_matrix(R)```
``` np.allclose(q, [0.19069, 0.43736, 0.87485, -0.083611])``` True ``` R = [[0.395, 0.362, 0.843, 0], [-0.626, 0.796, -0.056, 0],```
...      [-0.677, -0.498, 0.529, 0], [0, 0, 0, 1]]
``` q = quaternion_from_matrix(R)``` ``` np.allclose(q, [0.82336615, -0.13610694, 0.46344705, -0.29792603])```
True
``` R = random_rotation_matrix()``` ``` q = quaternion_from_matrix(R)```
``` is_same_transform(R, quaternion_matrix(q))``` True ``` R = euler_matrix(0.0, 0.0, np.pi/2.0)```
``` np.allclose(quaternion_from_matrix(R, isprecise=False),``` ...                quaternion_from_matrix(R, isprecise=True)) True 


---

## <kbd>function</kbd> `euler_from_quaternion`

```python
euler_from_quaternion(quaternion, axes='sxyz')
```

Return Euler angles from quaternion for specified axis sequence. 

``` angles = euler_from_quaternion([0.99810947, 0.06146124, 0, 0])```
``` np.allclose(angles, [0.123, 0, 0])``` True 


---

## <kbd>function</kbd> `euler_from_matrix`

```python
euler_from_matrix(matrix, axes='sxyz')
```

Return Euler angles from rotation matrix for specified axis sequence. 

axes : One of 24 axis sequences as string or encoded tuple 

Note that many Euler angle triplets can describe one matrix. 

``` R0 = euler_matrix(1, 2, 3, 'syxz')```
``` al, be, ga = euler_from_matrix(R0, 'syxz')``` ``` R1 = euler_matrix(al, be, ga, 'syxz')```
``` np.allclose(R0, R1)``` True ``` angles = (4*math.pi) * (np.random.random(3) - 0.5)```
``` for axes in _AXES2TUPLE.keys():``` ...    R0 = euler_matrix(axes=axes, *angles) ...    R1 = euler_matrix(axes=axes, *euler_from_matrix(R0, axes)) ...    if not np.allclose(R0, R1): print(axes, "failed") 


---

## <kbd>function</kbd> `quaternion_about_axis`

```python
quaternion_about_axis(angle, axis)
```

Return quaternion for rotation about axis. 

``` q = quaternion_about_axis(0.123, [1, 0, 0])```
``` np.allclose(q, [0.99810947, 0.06146124, 0, 0])``` True 


---

## <kbd>function</kbd> `rotation_matrix`

```python
rotation_matrix(axis, theta)
```

Return the rotation matrix associated with counterclockwise rotation about the given axis by theta radians. 


---

## <kbd>function</kbd> `rpytoQUAT`

```python
rpytoQUAT(r, p, y)
```






---

## <kbd>function</kbd> `rotation_from_matrix`

```python
rotation_from_matrix(matrix)
```

Return rotation angle and axis from rotation matrix. 

``` angle = (random.random() - 0.5) * (2*math.pi)```
``` direc = numpy.random.random(3) - 0.5``` ``` point = numpy.random.random(3) - 0.5```
``` R0 = rotation_matrix(angle, direc, point)``` ``` angle, direc, point = rotation_from_matrix(R0)```
``` R1 = rotation_matrix(angle, direc, point)``` ``` is_same_transform(R0, R1)```
True



---

## <kbd>function</kbd> `euler_from_rot_mat`

```python
euler_from_rot_mat(rot_mat)
```

Euler angles from rotation matrix ( 3x3 ) 



Parameters 
---------- rot_mat : TYPE  DESCRIPTION. 

Returns 
------- None. 


---

## <kbd>function</kbd> `inv_rpytoQUAT`

```python
inv_rpytoQUAT(r, p, y)
```

Takes euler angles and return a quaternion  




---

## <kbd>function</kbd> `q_mult`

```python
q_mult(q1, q2)
```








---

_This file was automatically generated via [lazydocs](https://github.com/ml-tooling/lazydocs)._
